My CV
=====

Building on Mac
---------------

1. Download and Install MacTex from https://tug.org/mactex/
2. Install all prompted updates for MacTex
3. Install Lyx from https://www.lyx.org/
4. Open `cv.lyx` file from `src` folder with Lyx, edit if needed and export PDF from there

If you want to create PDF from pure tex source then:
1. Configure MacTex pdflatex to use flag `-output-directory=../build`
2. Open `cv.tex` from `src` folder with TeXShop application, edit it if needed
3. Choose LaTeX as document typeset
4. Press Typeset button, grab cv.pdf from `build` folder

Please keep in mind that this source is no longer maintained.

